# alph.py Configuration
# ======================

# Web server root, in local filesystem terms.
docroot = "/var/www/html"

# Base URL of the alph.py installation, with no trailing slash.
urlroot = "http://localhost"


# LC_CTYPE for Unix tools. 
ctype = "en_US.UTF-8"

# Additional PATH string to append
extpath = ""





