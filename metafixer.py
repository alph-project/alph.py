#!/usr/bin/env python3
# Nasty metadata reformatter. Obliterates everything but title/desc.

import sys, os, json

def main(metafilename):
        
    if os.path.exists(metafilename):
        print(metafilename)
        metafile = open(metafilename,mode='r',encoding='utf-8')
        meta = json.load(metafile)
        metafile.close()
        
        ldmeta = {}

        if "links" in meta.keys():
            if isinstance(meta["links"],list):
                ldmeta["links"] = meta["links"]
                for link in ldmeta["links"]:
                    if "href" in link.keys():
                        link["body"] = link["href"]
                        del link["href"]

                    if "source" in link.keys():
                        link["body"] = link["source"]
                        del link["source"]

                        
                    if "spans" in link.keys():
                        link["target"] = {}
                        link["target"]["selector"] = []
                        for span in link["spans"]:
                            selector = {"type":"TextPositionSelector"}
                            selector["start"],selector["end"] = span.split("-")
                            link["target"]["selector"].append(selector)
                        del link["spans"]
                        
                    if "type" not in link.keys():
                        link["type"] = "unknown"
                    elif link["type"] == "hyperlink":
                        link["type"] = "deeplink"
                                                
            else:
                ldmeta["links"] = []
        else:
            ldmeta["links"] = []
            
        
        if "descriptor" not in meta.keys():
            ldmeta["descriptor"] = {}
        else:
            ldmeta["descriptor"] = meta["descriptor"]
            
        ldmeta["descriptor"]["@context"] = [
            "http://schema.org/",
            "http://alph.io/terms.jsonld" ]
        
        ldmeta["descriptor"]["@type"] = "MediaObject"
        
        if "id" in ldmeta["descriptor"].keys():
            del ldmeta["descriptor"]["id"]
        if "type" in ldmeta["descriptor"].keys():
            del ldmeta["descriptor"]["type"]
        if "alph:length" in ldmeta["descriptor"].keys():
            del ldmeta["descriptor"]["alph:length"]
        if "length" in ldmeta["descriptor"].keys():
            del ldmeta["descriptor"]["length"]
            
        # The fragmentBounds and mediaInterfaces links should all be removed now.
        oldFragBounds = "http://alph.io/fragmentBounds"
        oldInterfaces = "http://alph.io/mediaInterfaces"
        newFragBounds = "alph:fragmentBounds"
        newInterfaces = "alph:mediaInterfaces"
        
        
        if oldFragBounds in ldmeta["descriptor"].keys():
            del ldmeta["descriptor"][oldFragBounds]
            
        if oldInterfaces in ldmeta["descriptor"].keys():
            del ldmeta["descriptor"][oldInterfaces]

        if newInterfaces in ldmeta["descriptor"].keys():
            del ldmeta["descriptor"][newInterfaces]

        if newFragBounds in ldmeta["descriptor"].keys():
            del ldmeta["descriptor"][newFragBounds]


        if "type" in ldmeta["descriptor"].keys():
            ldmeta["descriptor"]["@type"] = ldmeta["descriptor"]["type"]
            del ldmeta["descriptor"]["type"]
            
        if "description" in meta.keys():
            ldmeta["descriptor"]["description"] = meta["description"]

        if "description" in ldmeta["descriptor"].keys():
            if isinstance(ldmeta["descriptor"]["description"],dict):
                newdesc = []
                for d in ldmeta["descriptor"]["description"].keys():
                    newdesc.append( { "@language":d,
                                      "@value":ldmeta["descriptor"]["description"][d]
                                      } )
                ldmeta["descriptor"]["description"] = newdesc
                    

        if "title" in meta.keys():
            ldmeta["descriptor"]["name"] = meta["title"]

        if "name" in ldmeta["descriptor"].keys():
            if isinstance(ldmeta["descriptor"]["name"],dict):
                newdesc = []
                for d in ldmeta["descriptor"]["name"].keys():
                    newdesc.append( { "@language":d,
                                      "@value":ldmeta["descriptor"]["name"][d]
                                      } )
                ldmeta["descriptor"]["name"] = newdesc

            
        # Write the changes in-place
        json.dump(ldmeta,open(metafilename,mode='w',encoding='utf-8'))

        print(json.dumps(ldmeta,ensure_ascii=False,indent=4))
        
main(sys.argv[1])
