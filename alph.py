#!/usr/bin/env python3
#
# alph.py - Alph Project server implementation prototype
#           https://gitlab.com/alph-project/alph.py
#           
#           The Alph Project <http://alph.io>


import sys, os, time, glob, cgi, html, json, datetime, subprocess, re

import Config
from Config import docroot, urlroot, ctype, extpath

from rdflib import Graph, plugin
from rdflib.serializer import Serializer

#import cgitb
#cgitb.enable(display=0, logdir="./dustbin/")

os.environ["LC_CTYPE"] = ctype
os.environ["PATH"] += extpath
lang = ctype.split(".")[0]

form = cgi.FieldStorage(keep_blank_values=True)
querystring = os.environ.get('QUERY_STRING')
debugtext = "" # + str(form.keys())
urlpath = os.environ.get('PATH_INFO')
docpath = docroot + urlpath
method = os.environ.get('REQUEST_METHOD')

SERVER_VER = "Alph-Version: 0\n"
LD_ADVERT = "Link: <http://www.w3.org/ns/ldp#Resource>; rel=\"type\"\n"

# -------------------------------------------------------------
def main():
    global debugtext

    if method == "GET" or method == "HEAD":
        return get()
    elif method == "POST":
        return post()
    elif method == "PUT":
        return put()
    elif method == "OPTIONS":
        return options()
    else:
        return errorResponse(405,"Method not supported. Sorry. :)")
    
# ----------------------------------------------------------------------
def newMeta(docpath):
    meta = {
        "descriptor":{
            "@context":["http://schema.org",
                        "http://alph.io/terms.jsonld"],
            "@type":"MediaObject"
            },
        "links":[]
        }

    if os.path.isfile(docpath):
        s = os.stat(docpath)
        meta["lastSize"] = s[6]

        meta["descriptor"] = makeMediaMeta(meta["descriptor"],docpath)
        
    return meta

def newDirMeta():
    # ???
    return {}

def writeMeta(meta,docpath):
    metafile = open(docpath + ".meta",mode='w+',encoding='utf-8')
    json.dump(meta,metafile)
    metafile.flush()
    os.fsync(metafile.fileno())
    metafile.close()

    
def secondsFromIsoTime(isotime):
    # Convert from ISO 8601 format (eg. "10:04:03.12345")
    splitduration = re.split("[^0-9+]",isotime)
    duration = (int(splitduration[0]) * 3600) + (int(splitduration[1]) * 60) + int(splitduration[2])
    if len(splitduration) > 3:
        duration = duration + float("0." + splitduration[3])
    return duration

def lastindexof(target,string):
    index = 0
    hit = -1
    for x in string:
        if x == target:
            hit = index
        index += 1
    return hit

def metamerge(meta,dirmeta):
    # Right now this quite crudely overlays the root-level properties of 'meta'
    # onto 'dirmeta'.
    for k in meta.keys():
        dirmeta[k] = meta[k]
        # If the value of the attribute is "!delete", we interpret this as a
        # request to remove the key entirely.
        if dirmeta[k] == "!delete":
            del dirmeta[k]
    return dirmeta
# GET -------------------------------------------------------------------    
def get():
    global debugtext
    global docpath
    global urlpath

    viewer = False
    
    if "REMOTE_USER" in os.environ:
        user = os.environ["REMOTE_USER"]
    elif "REDIRECT_REMOTE_USER" in os.environ:
        user = os.environ["REDIRECT_REMOTE_USER"]
    else:
        user = "public"

    # Does the requested file exist?
    
    if not os.path.exists(docpath):
        
        # If the client has requested an HTML document, but it
        # doesn't exist, see if the file WITHOUT the .html
        # extension exists; if so, this means the client wants a
        # viewer page for a media object.

        if docpath.endswith(".html") and os.path.isfile(docpath[0:-5]):
            viewer = True
            docpath = docpath[0:-5]
            urlpath = urlpath[0:-5]
        else:
            return errorResponse(404,"THE RESOURCE YOU HAVE REQUESTED -- NAMELY,\n\n    " + urlroot + urlpath + "\n\n-- APPEARS NOT TO EXIST ON THIS SERVER.\n\n\nWHILE WE ARE SURE THAT YOU FIND THIS\nTURN OF EVENTS WHOLLY UNSATISFACTORY,\nIT IS WITH OUR SINCEREST APOLOGIES\nTHAT WE MUST TELL YOU TO GO\nFRUMDINK YOUR HEEBAR, GURUNDINONK.\n\n--ERROR 404")

        
    if not os.path.isfile(docpath):

        # Request for a directory returns index.html if it exists; failing that,
        # an index page is generated.

        if os.path.exists(os.path.dirname(docpath) + "/index.html"):
            urlpath = urlpath + "index.html"
            docpath = docpath + "index.html"
        else:        
            return dirListing(user)

    # If a .meta file exists for the requested file, read it. If it
    # doesn't, generate it and write it to disk. Every file should have
    # a corresponding .meta file. This is generated when a file
    # is uploaded through PUT or POST, but not if someone uses 'scp'
    # or whatever to put files on the server.
    if os.path.exists(docpath+".meta"):
        meta = json.load(open(docpath + ".meta",encoding='utf-8'))
    else:
        meta = newMeta(docpath)
        writeMeta(meta,docpath)

        
    # Make sure the LD metadata points to the URL it's been requested for
    meta["descriptor"]["@id"] = urlroot + urlpath

    
    # If the file has been changed since the last meta file was
    # saved, regenerate the media metadata as the file may have
    # changed/been replaced.
    
    s = os.stat(docpath)
    if "lastSize" not in meta.keys() or s[6] != meta["lastSize"]:
        meta["lastSize"] = s[6]
        meta["descriptor"] = makeMediaMeta(meta["descriptor"],docpath)
        writeMeta(meta,docpath)

    
    # Get the directory meta, if there is any, and merge with the file
    # meta.
    
    meta["descriptor"] = compileParentMeta(meta["descriptor"])
    
        
    # *** While I'm still playing with metadata formatting, we generate
    # the media metadata on every request.
    #if "fileFormat" not in meta["descriptor"].keys():
    #    meta["descriptor"] = makeMediaMeta(meta["descriptor"],docpath)
    #    writeMeta(meta,docpath)

    if "HTTP_ACCEPT" in os.environ:
        accept = os.environ["HTTP_ACCEPT"]
    else:
        accept = "*"

        
    if ("describe" in form) or ("rdf" in accept) or ("ld+" in accept) or ("turtle" in accept) or ("ntriples" in accept):

        # Request for media metadata. Authorship information, type,
        # transclusion specs, etc...

        if "describe" in form.keys():
            if form["describe"].value == "regen":
                meta["descriptor"] = makeMediaMeta(meta["descriptor"],docpath)
                writeMeta(meta,docpath)

        # We'll also put in the server metadata here. I keep changing the format
        # for these properties/types, and it's a headache to have to reformat
        # the meta files.

        #meta["descriptor"]["alph:mediaInterfaces"] = [
        #    "http://alph.io/interfaces/fragment",
        #    "http://alph.io/interfaces/link",
        #    "http://alph.io/interfaces/describe"]

        #meta["descriptor"]["alph:fragmentBounds"] = {
        #    # Ugh... fill this in... needs to be set per-type.
        #}

        sendLD(meta,accept)
        
        return
    
    elif "editmeta" in form:
        
        return metaEditor(meta)
    
    elif "link" in form:
        
        if form["link"].value.startswith("http"):
            # This is a request to fire the backlinker on a specific web page.
            # As you wish!
            sys.stdout.buffer.write(SERVER_VER.encode("utf-8"))
            sys.stdout.buffer.write("Content-type: text/plain; charset=UTF-8\n\n".encode('utf-8'))
            linkerrun = subprocess.Popen(['./linker.py',urlpath,form["link"].value,"t"],stdout=subprocess.PIPE)
            linkeroutput = linkerrun.communicate()[0]
            
            sys.stdout.buffer.write(linkeroutput)
            return

        # ?link called without a value just dumps all of the links for this file
        linkout = []
        
        if "links" in meta.keys():
            if form["link"].value:
                # *** NEEDS WORK *** There should be a simple query
                # language for links that allow people to filter the
                # output. For now, the simplest possible query, is a
                # simple plain-text match against the stringified JSON
                # representation of each link.
                search = form["link"].value
                for link in meta["links"]:
                    if json.dumps(link).find(search) > 0:
                        linkout.append(link)
            else:
                linkout = meta["links"]

        # ---- Link meta pre-processing here? -----
        sys.stdout.buffer.write(SERVER_VER.encode("utf-8"))
        print("Content-type: application/json; charset=UTF-8\n\n")
        print(json.dumps(linkout))
        return
    elif "fragment" in form:
        return transclude(meta)
    else:
        # Either there was no query, or there was a malformed query sent.
        # Treat this as a direct resource request.
        mimestring = meta["descriptor"]["fileFormat"]
        
        # Generate a viewer/transclusion selector page?
        if viewer == True:
            if mimestring.startswith("image/"):
                return imageviewer(meta)
            elif mimestring.startswith("text/plain"):
                return textview(meta)
            elif mimestring.startswith("video/"):
                return videoviewer(meta)
            elif mimestring.startswith("audio/"):
                return audioviewer(meta)
            elif mimestring.startswith("application/alink"):
                return textview(meta)
            else:
                return blobviewer(meta)
            
        # For now, just barf the thing out.
        else:

            if mimestring.startswith("text/"):
                # Force UTF-8, because 'file' will assume ASCII if no
                # non-ASCII characters are present in the file
                mimestring = mimestring.split(";")[0]
                mimestring += "; charset=UTF-8"

            header = "Content-type:" + mimestring + "\n\n"
            
            sys.stdout.buffer.write(SERVER_VER.encode("utf-8"))
            sys.stdout.buffer.write(LD_ADVERT.encode("utf-8"))            
            sys.stdout.buffer.write(header.encode('utf-8'))
        
            if mimestring.startswith("text/html"):
                sys.stdout.buffer.write(open(docpath,mode='rb').read())
            else:
                sys.stdout.buffer.write(open(docpath,mode='rb').read())

        # Do backlink hunt...
        hunt()
        return

        
# POST -------------------------------------------------------------------    
def post():
    global debugtext
    if "REMOTE_USER" in os.environ:
        user = os.environ["REMOTE_USER"]
    elif "REDIRECT_REMOTE_USER" in os.environ:
        user = os.environ["REDIRECT_REMOTE_USER"]
    else:
        textResponse("Can't POST as unauthenticated user.")
        # This shouldn't even come up, as Apache should be configured to require
        # auth for all POST requests  .
        user = "public"

    if not os.path.isfile(docpath):
        # Not a file, so is it a directory? You can post metadata to a
        # directory, but that's it. See if that's what we're doing
        if os.path.isdir(docpath):
            if "describe" not in form:
                # Just show the directory listing.
                return dirListing(user)
            # Now post the metadata...
            # *** IMPLEMENT THIS ***
            return
        # Is it a completely nonexistant filename?
        if not os.path.exists(docpath):
            # Create it if the parent directory exists?
            parentdir = docpath[0:lastindexof("/",docpath)]
            if os.path.isdir(parentdir):
                return postFile(newMeta(docpath))
            else:
                return textResponse("Cannot create file in nonexistant folder.")

    # If a .meta file exists for the requested file, read it. If it
    # doesn't, generate it.
    if os.path.exists(docpath+".meta"):
        meta = json.load(open(docpath + ".meta",encoding='utf-8'))
    else:
        meta = newMeta(docpath)
        writeMeta(meta,docpath)        

        
    # ***
    # There should be some clever logic here to merge certain meta
    # properties from the containing folder, and further up the tree
    # as well, perhaps.
    # ***
    
    if "describe" in form:
        # POSTing new/updated metadata. Expect JSON. Do some kind of merge.
        # *** VERY CRUDE AT THE MOMENT ***
        formmeta = json.loads(form["describe"].value)
        meta["descriptor"] = metamerge(formmeta,meta["descriptor"])
        json.dump(meta,open(docpath + ".meta",mode='w+',encoding='utf-8'))
        #return textResponse(form["describe"].value)
        return get()
    elif "link" in form:
        # POSTing a new/updated link. Expect JSON. Do some kind of merge.
        #
        # *** Not implemented yet.
        #
        # For now, return the unmodified metadata.
        return get()
    else:
        # Okay, new data is being appended to an existing file, or a new file
        # is being created.
        
        # Regnerate media meta, JUST IN CASE there's been a change to the
        # file that somehow was not recorded in the metadata.
        meta["descriptor"] = makeMediaMeta(meta["descriptor"],docpath)
        
        # For now, just make this work for text.
        if meta["descriptor"]["fileFormat"].startswith("text/plain") or meta["descriptor"]["fileFormat"].startswith("inode/x-empty"):
            appendPostedText(meta)
        elif meta["descriptor"]["fileFormat"].startswith("text/html"):
            # We don't append to HTML files. POSTs to HTML files will REPLACE the
            # existing file. 
            postFile(meta)
        elif meta["descriptor"]["fileFormat"].startswith("application/alink"):
            modifyAlink(meta)
        else:
            return textResponse("Sorry, POSTing to existing resources of this media type is not currently supported.\n\nIf you are trying to over-write the resource, use the PUT methos instead.")

def put():
    # The PUT request will create a new file on the server.  
    # When a request is sent with "asdir" in the query string, a directory will
    # be created instead of a file. Ex.:
    #
    # HTTP
    global debugtext
    if "REMOTE_USER" in os.environ:
        user = os.environ["REMOTE_USER"]
    elif "REDIRECT_REMOTE_USER" in os.environ:
        user = os.environ["REDIRECT_REMOTE_USER"]
    else:
        textResponse("Can't PUT as unauthenticated user.")
        # This shouldn't even come up, as Apache should be configured to require
        # auth for all POST requests  .
        user = "public"

    if not os.path.isfile(docpath):
        # Not a file, so is it a directory? We can't PUT on top of extant directories.
        if os.path.isdir(docpath):
            return textResponse("Resource already exists as a directory. You cannot PUT here.")
        # Is it a completely nonexistant filename?
        if not os.path.exists(docpath):
            # Create it if the parent directory exists?
            parentdir = docpath[0:lastindexof("/",docpath)]
            if os.path.isdir(parentdir):
                if "asdir" in form:
                    # Create directory
                    os.mkdir(docpath)
                    resp = "Content-type: text/html; charset=UTF-8\n"
                    resp += "Refresh: 3; url=" + urlroot + urlpath + "\n\n"
                    resp += "<!DOCTYPE html><html><head>"
                    resp += '<meta http-equiv="refresh" content="3;'
                    resp += urlroot + urlpath + '"></head><body>'
                    resp += "Directory created. Reloading in 3..."
                    resp += '</body></html>'
                    print(resp)
                    return
                else:
                    # Create file
                    return postFile(newMeta(docpath))
            else:
                return textResponse("Cannot PUT file in nonexistant folder.")
    else:
        # At this point, we're trying to PUT to a filename that
        # already exists.  PUT will OVERWRITE existing files, not append
        # to them (see post()) We will try to preserve metadata, though.

        if os.path.exists(docpath+".meta"):
            meta = json.load(open(docpath + ".meta",encoding='utf-8'))
        else:
            meta = newMeta(docpath)

        postFile(meta)
        
def getUser():
    if "REMOTE_USER" in os.environ:
        user = os.environ["REMOTE_USER"]
    elif "REDIRECT_REMOTE_USER" in os.environ:
        user = os.environ["REDIRECT_REMOTE_USER"]
    else:
        user = "public"
    return user

def options():
    user = getUser()
    if user == "public":
        resp = "Allow: OPTIONS, HEAD, GET, POST, PUT\n"
    else:
        if os.path.isfile(docpath):
            resp = "Allow: OPTIONS, HEAD, GET, POST, PUT\n"
        elif os.path.isdir(docpath):
            resp = "Allow: OPTIONS, HEAD, GET, POST, PUT\n"
    sys.stdout.buffer.write(resp.encode('utf-8'))
    return textResponse("OPTIONS sent. Check yo' headers, " + user)
                           
def setEphemeralMeta(meta):
    meta["descriptor"]["alph:interface"] = [
        "http://alph.io/interfaces/fragment",
        "http://alph.io/interfaces/describe",
        "http://alph.io/interfaces/link" ]
    return meta

def sendLD(meta,serialization):

    # Output Linked Data for the media resource.

    # THIS shouldn't be here, probably. Should be in makeMediaMeta()
    if "image/" in meta["descriptor"]["fileFormat"]: 
        meta["descriptor"]["@type"] = "ImageObject"
    elif "audio/" in meta["descriptor"]["fileFormat"]:
        meta["descriptor"]["@type"] = "AudioObject"

    # Or should it? Shunt in our interface support here, I guess.
    meta = setEphemeralMeta(meta)
    
    sys.stdout.buffer.write(SERVER_VER.encode("utf-8"))
    sys.stdout.buffer.write(LD_ADVERT.encode("utf-8"))
        
    if serialization == "text/rdf+n3":
        
        # UGLY HACK -- fix this. The JSON-LD parser gets confused by
        # fileFormat, thinking it's a relative URI. So... remove it.
        del meta["descriptor"]["fileFormat"]
        g = Graph().parse(data = json.dumps(meta["descriptor"]),format = 'json-ld')
        sys.stdout.buffer.write("Content-Type: text/rdf+n3; charset=UTF-8\n\n".encode("utf-8"))
        sys.stdout.buffer.write(g.serialize(format='n3',encoding="utf-8"))
        
    elif serialization == "application/ntriples":
        
        del meta["descriptor"]["fileFormat"]
        g = Graph().parse(data = json.dumps(meta["descriptor"]),format = 'json-ld')
        sys.stdout.buffer.write("Content-Type: application/ntriples; charset=UTF-8\n\n".encode("utf-8"))
        sys.stdout.buffer.write(g.serialize(format='nt',encoding="utf-8"))
        
    elif serialization == "text/turtle":
        
        del meta["descriptor"]["fileFormat"]
        g = Graph().parse(data = json.dumps(meta["descriptor"]),format = 'json-ld')
        sys.stdout.buffer.write("Content-Type: application/x-turtle; charset=UTF-8\n\n".encode("utf-8"))
        sys.stdout.buffer.write(g.serialize(format='turtle'))
    elif serialization == "application/rdf+xml":
        del meta["descriptor"]["fileFormat"]
        g = Graph().parse(data = json.dumps(meta["descriptor"]),format = 'json-ld')
        sys.stdout.buffer.write("Content-Type: application/rdf+xml; charset=UTF-8\n\n".encode("utf-8"))
        sys.stdout.buffer.write(g.serialize(format='xml',encoding="UTF-8"))
    else:
        print("Content-type: application/ld+json; charset=UTF-8\n\n")
        print(json.dumps(meta["descriptor"],sort_keys=True))
        
        return
        
def transclude(meta):
    
    spans = form["fragment"].value.split("`")
    
    if meta["descriptor"]["fileFormat"].startswith("text"):
        transcludeText(meta,spans)
    elif meta["descriptor"]["fileFormat"].startswith("image"):
        # Raster images and vector images need to be handled differently.
        transcludeRaster(meta,spans)
    elif meta["descriptor"]["fileFormat"].startswith("audio"):
        transcludeAudio(meta,spans)
    elif meta["descriptor"]["fileFormat"].startswith("video"):
        transcludeVideo(meta,spans)
    else:
        # Fall back to binary blob transclusion
        transcludeBinary(meta,spans)

def modifyAlink(meta):
    global debugtext
    ldcontext = "http://alph.io/terms.jsonld"

    # Open the alink file, parse it, give us something to work with.
    alink = json.load(open(docpath,encoding='utf-8'))

    # Parse the posted media into a usable object.
    # *** We really need more input validation, here. ***
    postdata = form["media"]
    if postdata.file:
        if isinstance(postdata.value, str):
            plink = json.loads(postdata.file.read())
        else :
            plink = json.loads(postdata.file.read().decode("utf-8"))

    del plink["@context"]
    
    # If the link file is a single link, turn it into a link set
    if "@graph" not in alink:
        del alink["@context"]
        alink = { "@context": ldcontext,
                  "@graph" : [ alink ] }

    appendLink = True

    # Find the link that matches the posted link, if it exists  
    for i,l in enumerate(alink["@graph"]):
        if l["id"] == plink["id"]:
            appendLink = False
            alink["@graph"][i] =  plink
            break

    if appendLink:
        alink["@graph"].append(plink)

    # Why do I even support single links? I dunno. But, if this is
    # a one-item list, just make it a single link again before
    # saving to disk.
    if len(alink["@graph"]) == 1:
        alink = alink["@graph"][0]
        alink["@context"] = ldcontext

    linkfile = open(docpath, mode="w", encoding='utf-8')
    json.dump(alink,linkfile)
    linkfile.flush()
    os.fsync(linkfile.fileno())
    linkfile.close()

    return get()
    
            
def appendPostedText(meta):
    global debugtext

    # TODO Check the size of the posted media. Too big? Bail out.
    
    postdata = form["media"]
    if postdata.file:
        if isinstance(postdata.value, str ):
            media = postdata.file.read()
        else:
            media = postdata.file.read().decode("utf-8")
            
    mediafile = open(docpath,mode="a",encoding='utf-8')
    oldlength = meta["descriptor"]["alph:textLength"]
    if "ts" in form:
        # User has provided a timestamp string to be inserted before the
        # rest of the text
        oldlength += mediafile.write(form["ts"].value)
    lengthadded = mediafile.write(media)

    responseobject = {}
    responseobject["src"] = urlroot + urlpath
    responseobject["origin"] = oldlength
    responseobject["extent"] = oldlength + lengthadded
    responseobject["href"] = urlroot + urlpath + "?fragment=" + str(responseobject["origin"]) + "-" + str(responseobject["extent"])
    sys.stdout.buffer.write(SERVER_VER.encode("utf-8"))
    print("Content-Type: application/json; charset=UTF-8\n\n")
    print(json.dumps(responseobject))

    # update and save the media metadata
    meta["descriptor"] = makeMediaMeta(meta["descriptor"],docpath)
    json.dump(meta,open(docpath + ".meta",mode='w+',encoding='utf-8'))
    return

def postFile(meta):
    global debugtext

    binary = False
    if "binary" in form.keys():
        binary = True

    # TODO: Check the size of the posted media. Too big? Bail out.
        
    postdata = form["media"]

    if postdata.file:
        if binary:
            media = postdata.file.read()
            # Check to see if this is actually a string
            if isinstance(postdata.value, str):
                mediafile = open(docpath,mode="w",encoding='utf-8')
            else:
                mediafile = open(docpath,mode="wb")
        else:
            if isinstance(postdata.value, str ):
                media = postdata.file.read()
                #if not os.path.exists(docpath):
                    # Create header for newly-created text file
                    # If a new file is being posted and you don't want a header like
                    # this to be generated, post it as binary. 
                    # media = "Content-type: text/plain; encoding=utf-8\nDate: " + time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime()) + "\n" + "URL: " + urlroot + urlpath + "\n\n" + media

            else:
                media = postdata.file.read().decode("utf-8")
                
            mediafile = open(docpath,mode="w+",encoding='utf-8')

    # Write the file and flush the buffer, because we need to read the
    # file to generate its metadata.
    
    mediafile.write(media)
    mediafile.flush()
    os.fsync(mediafile.fileno())
    
    meta["descriptor"] = makeMediaMeta(meta["descriptor"],docpath)
    writeMeta(meta,docpath)
    
    # Return the description of the newly-created media.
    sys.stdout.buffer.write(SERVER_VER.encode("utf-8"))
    sys.stdout.buffer.write(LD_ADVERT.encode("utf-8"))    
    print("Content-Type: application/ld+json; charset=UTF-8\n\n")
    print(json.dumps(meta))

    return

def transcludeText(meta,rawspans):
    # TODO: Do this better. It works, but relying on Awk is
    # ...inelegant, to say the least. Or maybe it isn't; maybe Awk is
    # *the* perfect tool for the job, but ...yeesh. I can't believe
    # getting a Unicode substring in Python is such a fucking
    # headache.
    
    global debugtext

    spans = []
    for ix in rawspans:
        matches = re.search('[\d]*-[\d]*',ix)
        if matches:
            regexmatch = matches.group(0)
        else:
            debugtext += "\nInvalid transclusion spec: '" + ix + "'.\n"
            return textResponse(debugtext)
        
        if len(regexmatch) == len(ix):
            spans.append(ix)
        else:
            debugtext += "\nAnomalous characters in transclusion spec '" + ix + "'.\n"
            return textResponse(debugtext)

    if len(spans) < 1:
        debugtext += "\nNo valid transclusion specifiers found in this string:" + args["fragment"].value + ".\n"
        return textResponse(debugtext)

    transclusion = bytearray()

    # ↓ These don't/won't work. See note (*)
    #infile = open(docpath,mode='r',encoding='utf-8')
    #infile = open(docpath,mode='r',encoding='utf-8').read()

    # Here's another annoying thing: BOM check.
    bomcheck = open(docpath,'rb').read(3)
    if bomcheck == b'\xef\xbb\xbf':
        bom = 1
    else: bom = 0
    
    for spanstring in spans:
        span = spanstring.split("-")
        origin = min(int(span[0]),int(span[1]))
        extent = max(int(span[0]),int(span[1]))
        length = extent - origin
        oldlength = int(meta["descriptor"]["alph:textLength"])
        if origin > oldlength or extent > oldlength:
            debugtext += "\nTransclusion spec out of bounds. Source text is " + str(oldlength) + " code points long.\n"
            return textResponse(debugtext)
        else:
            # (*)
            # *sigh*  This is fucking annoying. Even when a file is opened with utf-8
            # encoding, seek() and read() still work on bytes, not codepoints. So this
            # doesn't work:
            #    infile.seek(origin,0)
            #    transclusion += infile.read(length)
            # Furthermore, you'd think that after reading the file into a string
            # then you'd be able to substr() -- oh, wait, Python doesn't have substr()
            # -- okay, well, you'd think that you'd be able to slice the Unicode string
            # AS TEXT, right? I mean, we loaded the thing AS TEXT; theoretically it would
            # then be treated AS TEXT, right? Nope. So, this doesn't work either:
            #     transclusion += infile[origin:length]
            #
            # I ~could~ write my own function for counting chars/codepoints, but, y'know,
            # this is a one-line Awk program, so ... let's just do a one-line Awk program.

            # Note: Awk indexes strings from 1, not 0
            awkstring = 'BEGIN {RS="\x04";ORS=""}{print substr($0,' + str(origin+1+bom) + "," + str(length) + ");}"
            awk = subprocess.Popen(['awk',awkstring,docpath],stdout=subprocess.PIPE)
            transclusion += redactText(awk.communicate()[0],meta,origin,origin+length)
            
    respheader = "Content-type: " + meta["descriptor"]["fileFormat"] + "\n\n"
    sys.stdout.buffer.write(SERVER_VER.encode("utf-8"))
    sys.stdout.buffer.write(LD_ADVERT.encode("utf-8"))    
    sys.stdout.buffer.write(respheader.encode('utf-8'))
    sys.stdout.buffer.write(transclusion)

    # Now, dispatch the backlink hunter...
    hunt()
    
    return

def redactText(string,meta,origin,extent):
    
    return string

def transcludeRaster(meta,rawspans):

    global debugtext

    # Raster transclusion doesn't support concatenation like text, audio, etc;
    # So if we were sent more than one span, return an error.
    if len(rawspans) > 1:
        debugtext += "\nImage transclusions do not support concatenation."
        return textResponse(debugtext)

    # get fragment syntax if present
    rawspan = rawspans[0].split(":")
    if len(rawspan) > 1:
        syntax = rawspan[0]
        rawspan = rawspan[1]
    else:
        rawspan = rawspan[0]

    # For now, ignore fragment syntaxes and just implement the two implied syntaxes.
    matches = re.search('\d*\.\d*,\d*\.\d*-\d*\.\d*,\d*\.\d*',rawspan) # floatrect
    if matches:
        syntax = "floatrect"
    else:
        matches = re.search('\d*,\d*-\d*,\d*',rawspan) # pixelrect
        if matches:
            syntax = "pixelrect"
        else:
            debugtext += "\nUnknown fragment syntax: '" + rawspan + "'.\n"
            return textResponse(debugtext)
        
    regexmatch = matches.group(0)
    if len(regexmatch) != len(rawspan):
        debugtext += "\nAnomalous characters in " + syntax + " fragment spec '" + rawspan + "'.\n"
        return textResponse(debugtext)

    coords = rawspan.split("-")
    originX,originY = coords[0].split(",")
    extentX,extentY = coords[1].split(",")

    metawidth = int(meta["descriptor"]["width"].split()[0])
    metaheight = int(meta["descriptor"]["height"].split()[0])
    
    if syntax == "floatrect":
        # Convert normalized float values to pixel values
        originX = int(float(originX) * metawidth)
        originY = int(float(originY) * metaheight)
        extentX = int(float(extentX) * metawidth)
        extentY = int(float(extentY) * metaheight)
    else:
        # Convert to ints
        originX = int(originX)
        originY = int(originY)
        extentX = int(originX)
        extentY = int(originY)
        
    # Flip coordinates if they're backwards
    if extentX < originX and extentY < originY:
        originX, extentX = extentX, originX
        originY, extentY = extentY, originY

    width = extentX - originX
    height = extentY - originY

    sys.stdout.buffer.write(SERVER_VER.encode("utf-8"))
    sys.stdout.buffer.write(LD_ADVERT.encode("utf-8"))
    
    # Bounds check
    if extentX > metawidth or extentY > metaheight:
        debugtext = "Error: " + syntax + " extents are out of bounds.\n"
        debugtext += "Image dimensions are: " + str(metawidth) + "×" + str(metaheight)
        debugtext += "; you requested " + str(originX) + "," + str(originY) + "-" + str(extentX) + "," + str(extentY) + "."
        return textResponse(debugtext)
    else:
        # I'm cheating, here. We should probably return whatever format the
        # image was originally in, or whatever format is requested in the query
        # string. For now, though, we'll just do PNG on all requests.
        
        respheader = "Content-type: image/png\n\n"
        # Now... here I am spawning Unix utilities again. And why?
        # Because I can't seem to find a way to make Python's print()
        # or sys.stdout.buffer.write() functions finish before the
        # 'convert' subprocess, so the image data starts being
        # transmitted before the HTTP header.
        p = subprocess.Popen(['echo','-en',respheader])
        p.wait()
        p.communicate()
        geometry = str(width) + "x" + str(height) + "+" + str(originX) + "+" + str(originY)
        magick = subprocess.Popen(['convert','-crop',geometry,docpath,'png:-'], shell=False)
        magick.communicate()
        
    # Now, dispatch the backlink hunter...
    hunt()
    
    return
def transcludeVideo(meta,rawspans):
    # ***********************************************
    # *** THIS ONLY SUPPORTS WEBM VIDEO RIGHT NOW ***
    # ***********************************************
    global debugtext
    
    # Video transclusion doesn't support concatenation ...yet.  So if
    # we were sent more than one span, return an error.
    if len(rawspans) > 1:
        debugtext += "\nVideo transclusion does not currently support concatenation."
        return textResponse(debugtext)

    # However, video (which is usually video+audio) transclusion
    # ~will~ support channel specifiers, so...
    rawspan = rawspans[0].split(":")
    if len(rawspan) > 1:
        layer = rawspan[0]
        rawspan = rawspan[1]
    else:
        rawspan = rawspan[0]

    # We're just going to ignore channels in this version, though.

    # Validate transclusion spec: {int frame}-{int frame}
    match = re.search('\d*\.\d*-\d*\.\d*',rawspan) # This is for unit seconds
    #match = re.search('\d*-\d*',rawspan) # This is for unit frames
    if match:
        regexmatch = match.group(0)
    else:
        debugtext += "\nInvalid transclusion spec: '" + rawspan + "'.\n"
        return textResponse(debugtext)
        
    if len(regexmatch) != len(rawspan):
        debugtext += "\nAnomalous characters in transclusion spec '" + rawspan + "'.\n"
        return textResponse(debugtext)

    # This is SO MUCH EASIER if I use floating-point seconds as the transclusion
    # unit, BUT I DON'T WANT TO.  I want to use frames. Because frames are discrete,
    # and seconds are not.
    span = rawspan.split("-")
    origin = min(float(span[0]),float(span[1]))  # Frames
    extent = max(float(span[0]),float(span[1]))  # Frames

    length = extent - origin
    # Convert those frame values to seconds
    
    #rate = 1 / meta["frame_rate"]
        
    #origin = origin * rate
    #extent = extent * rate
    #length = length * rate

    # Get video duration
    #mediaduration = float(meta["format"]["duration"]) # Seconds
    
    # Convert from ISO 8601 format
    mediaduration = secondsFromIsoTime(meta["descriptor"]["duration"])
    
    # Bounds check
    if origin > mediaduration or extent > mediaduration:
        debugtext += "\nTransclusion spec out of bounds. Source video is " + str(mediaduration) + " seconds long.\n" # Seconds
        return textResponse(debugtext)
    else:
        origin = str(origin)
        extent = str(extent)
        length = str(length)
        #debugtext += "Origin: " + origin + " Extent: " + extent + " (" + length + ")"
        #return textResponse(debugtext)

        # Do some checks here to see if the video file is in a
        # streamable format. If so, do a stream copy; if not
        # ...convert to a streamable format?

        # For now, just assume everything is a WebM and everything's great.

        respheader = "Content-type: video/webm\n\n"
        p = subprocess.Popen(['echo','-en',respheader])
        p.wait()
        p.communicate()

        # The tricky part of this avconv command is the '-itsoffset' parameter.
        # I'm not exactly sure how it works, but if you don't set the itsoffset
        # parameter to the origin offset, the length of video you get back is
        # ...not right. 
        avconv = subprocess.Popen(['ffmpeg',
                                   '-ss',origin, '-itsoffset',origin,
                                   '-t',length,
                                   '-i',docpath,
                                   '-c','copy', #'-c:v','libvpx','-c:a','libopus',
                                   '-f','webm',
                                   'pipe:1'], shell=False)
        avconv.communicate()

    # Now, dispatch the backlink hunter...
    hunt()
    
    return
    
def transcludeAudio(meta,rawspans):
    # ******************************************************
    # *** THIS ONLY SUPPORTS WEBM/VORBIS AUDIO RIGHT NOW ***
    # ******************************************************
    
    global debugtext
    
    # Audio transclusion doesn't support concatenation ...yet.  So if
    # we were sent more than one span, return an error.
    if len(rawspans) > 1:
        debugtext += "\nVideo transclusion does not currently support concatenation."
        return textResponse(debugtext)

    # However, audio transclusion ~will~ support channel specifiers, so...
    rawspan = rawspans[0].split(":")
    if len(rawspan) > 1:
        layer = rawspan[0]
        rawspan = rawspan[1]
    else:
        rawspan = rawspan[0]

    # We're just going to ignore channels in this version, though.

    # Validate transclusion spec: {int frame}-{int frame}
    match = re.search('\d*\.\d*-\d*\.\d*',rawspan) # This is for unit seconds
    if match:
        regexmatch = match.group(0)
    else:
        debugtext += "\nInvalid transclusion spec: '" + rawspan + "'.\n"
        return textResponse(debugtext)
        
    if len(regexmatch) != len(rawspan):
        debugtext += "\nAnomalous characters in transclusion spec '" + rawspan + "'.\n"
        return textResponse(debugtext)

    span = rawspan.split("-")
    origin = min(float(span[0]),float(span[1]))  
    extent = max(float(span[0]),float(span[1]))  

    length = extent - origin

    # Get audio duration 
    #mediaduration = float(meta["format"]["duration"]) # Seconds
    mediaduration = secondsFromIsoTime(meta["descriptor"]["duration"])
    
    # Bounds check
    if origin > mediaduration or extent > mediaduration:
        debugtext += "\nTransclusion spec out of bounds. Source audio is " + str(mediaduration) + " seconds long.\n" # Seconds
        return textResponse(debugtext)
    else:
        origin = str(origin)
        extent = str(extent)
        length = str(length)
        #debugtext += "Origin: " + origin + " Extent: " + extent + " (" + length + ")"
        #return textResponse(debugtext)

        # Do some checks here to see if the audio file is in a
        # streamable format. If so, do a stream copy; if not
        # ...convert to a streamable format?

        respheader = "Content-type: audio/webm\n\n"
        p = subprocess.Popen(['echo','-en',respheader])
        p.wait()
        p.communicate()

        # The tricky part of this avconv command is the '-itsoffset' parameter.
        # I'm not exactly sure how it works, but if you don't set the itsoffset
        # parameter to the origin offset, the length of video you get back is
        # ...not right. 
        avconv = subprocess.Popen(['ffmpeg',
                                   '-ss', origin, '-itsoffset',origin,
                                   '-t',length,
                                   '-i',docpath,
                                   '-c','copy', 
                                   '-f','webm',
                                   'pipe:1'], shell=False)
        avconv.communicate()

    # Now, dispatch the backlink hunter...
    hunt()
    return

def transcludeBinary(meta,rawspans):
    # Text ~should~ be this easy.
    # * * * * * * * * * * *
    # * * * DO THIS * * * *
    # * * * * * * * * * * *
    print("Content-type: application/octet-stream\n\n")
    return

def dirListing(user):
    dirname = os.path.dirname(docpath)
    files = glob.glob(dirname + "/*")
    files.sort()

    # To authenticate a user who's looking at a directory index, we
    # can just refer them to the index's address, but with an HTTP
    # POST instead of GET. All POSTs need to be authenticated, so
    # they're prompted for credentials. The authenticated user name
    # will come back in REDIRECT_REMOTE_USER instead of REMOTE_USER,
    # so we need to look for that here.
    
    #if "REDIRECT_REMOTE_USER" in os.environ.keys():
    #    user = os.environ["REDIRECT_REMOTE_USER"]
    #if "REMOTE_USER" in os.environ:
    #    user = os.environ["REMOTE_USER"]
    #elif "REDIRECT_REMOTE_USER" in os.environ:
    #    user = os.environ["REDIRECT_REMOTE_USER"]

    # If they can't provide credentials, they get an error. If they
    # can, they get an index page with some tools.
    
    if user == "public":
        template = open('templates/listing-pub.html',mode='r',encoding='utf-8').read()
    else:
        template = open('templates/listing.html',mode='r',encoding='utf-8').read()

    template = template.replace('<!-- URLPATH -->',urlpath)
    template = template.replace('<!-- URLROOT -->',urlroot)

    if urlpath != '/':
        template = template.replace('<!-- PARENTLINK -->',
                                    '<a href="' + urlroot + urlpath + '..">&larr;</a> ')

    # Now generate the guts of an HTML table for the directory listing.
    
    codestring = " "
    
    ftemplate = "<tr><td><a href='!FURL'>!FNAME</a>!VIEWER</td><td class='mimetype'>!FTYPE</td><td>!TITLE!DESC</td></tr>"
    dtemplate = "<tr class='dir'><td class='dirname'><a href='!FURL'>!FNAME</a>!VIEWER</td><td class='mimetype'>!FTYPE</td><td>!TITLE!DESC</td></tr>"
    
    if len(files) > 0:
        for ix in files:
            
            ixisdir = os.path.isdir(ix)
            
            # Skip (hide) .meta files ...and Emacs temp files.
            if not ix.endswith(".meta") and not ix.endswith("~"):
                
                # Read the file/dir's descriptor, or create a dummy descriptor if
                # the meta file doesn't exist.
                if os.path.exists(ix + ".meta"):
                    fmeta = json.load(open(ix+".meta" ,encoding='utf-8'))
                elif ixisdir:
                    if os.path.exists(ix + "/.meta"):
                        fmeta = json.load(open(ix+"/.meta",encoding='utf-8'))
                    else:
                        fmeta = newDirMeta()
                else:
                    fmeta = newMeta(docpath)

                # Now build the table row...
                furl = urlroot + urlpath + os.path.basename(ix) + ("/" if ixisdir else "")
                fname = os.path.basename(ix) + ("/" if ixisdir else "")

                # Include a link to a viewer page?
                if not os.path.isfile(ix + ".html") and not ix.endswith(".html") and not os.path.isdir(ix):
                    viewer = '<small>(<a href="' + os.path.basename(ix) + '.html">html</a>)</small>'
                else:
                    viewer = ""

                # MIME type
                if ixisdir:
                    ftype = "(dir)"
                else:
                    if "fileFormat" in fmeta["descriptor"].keys():
                        ftype = fmeta["descriptor"]["fileFormat"].split(";")[0]
                        # Bit of a hack, here but... let's sneak the media type
                        # into the anchor as well, via the furl
                        furl = furl + "' type='" + ftype
                    else:
                        ftype = ""

                # Title (name)
                title = ""
                if not ixisdir:
                    if "name" in fmeta["descriptor"].keys():
                        namemeta = fmeta["descriptor"]["name"]
                        if isinstance(namemeta,list):
                            for n in namemeta:
                                title += '<div class="title"><small><sup>' + n["@language"] + ":</sup></small>" + n["@value"] + '</div>'
                        else:
                            title = '<div class="title">' + str(namemeta) + '</div>'

                # Description
                desc = ""
                if not ixisdir:
                    if "description" in fmeta["descriptor"].keys():
                        descmeta = fmeta["descriptor"]["description"]
                        if isinstance(descmeta,list):
                            for n in descmeta:
                                desc += '<div class="description"><small><sup>' + n["@language"] + ":</sup></small>" + n["@value"] + '</div>'
                        else:
                            desc += '<div class="description">' + str(descmeta) + '</div>'

                tablerow = dtemplate if ixisdir else ftemplate
                
                tablerow = tablerow.replace('!FURL',furl)
                tablerow = tablerow.replace('!FNAME',fname)
                tablerow = tablerow.replace('!VIEWER',viewer)
                tablerow = tablerow.replace('!FTYPE',ftype)
                tablerow = tablerow.replace('!TITLE',title)
                tablerow = tablerow.replace('!DESC',desc)

                codestring += tablerow
                
    template = template.replace('<!-- TABLEBODY -->',codestring)

    # Look for a '.dirinfo' file, which will contain a text/HTML fragment to be inserted
    # at the top of the generated listing.
    if os.path.exists(docpath + '/.dirinfo'):
        template = template.replace('<!-- DIRINFO -->',
                                    open(docpath + '/.dirinfo',
                                         mode='r',encoding='utf-8').read())
        
    respheader = SERVER_VER + "Content-type: text/html; charset=UTF-8\n\n"
    sys.stdout.buffer.write(respheader.encode('utf-8'))
    sys.stdout.buffer.write(template.encode('utf-8'))

def textview(meta):
    # Generate a viewer page for a text resource.

    descriptor = meta["descriptor"]
    
    template = open('templates/text.html',mode='r',encoding='utf-8').read()

    byline = "<p>Author: --AUTHORS--</p>"
    authortemplate = "<a href='--URL--'>--NAME--</a>"
    
    
    template = template.replace('--SRC--',descriptor["@id"])
    template = template.replace('--EXTENT--',str(descriptor["alph:textLength"]))
    template = template.replace('--TEXT--',html.escape(open(docpath,mode="r",encoding="utf-8").read()))

    # There's got to be a smarter way to do this...
    # NAME ---
    
    if "name" in descriptor:
        if isinstance(descriptor["name"], str):
            template = template.replace('--NAME--', descriptor["name"])
            template = template.replace('<!-- TITLE -->', descriptor["name"] + " (" + descriptor["@id"] + ")")
        else:
            if "@language" in descriptor["name"]:
                template = template.replace('--NAME--', descriptor["name"]["@value"])
                template = template.replace('<!-- TITLE -->', descriptor["name"]["@value"] + " (" + descriptor["@id"] + ")")
            else:
                firstlang = descriptor["name"][0]
                template = template.replace('--NAME--', firstlang["@value"])
                template = template.replace('<!-- TITLE -->', firstlang["@value"] + " (" + descriptor["@id"] + ")")
    else:
        template = template.replace('--NAME--', descriptor["@id"])
        template = template.replace('<!-- TITLE -->', descriptor["@id"])

    # DESCRIPTION ---
    
    if "description" in descriptor:
        if isinstance(descriptor["description"], str):
            template = template.replace('--DESC--', descriptor["description"])
        else:
            if "@language" in descriptor["description"]:
                template = template.replace('--DESC--', descriptor["description"]["@value"])
            else:
                firstdesc = descriptor["description"][0]
                template = template.replace('--DESC--', firstdesc["@value"])
    else:
        template = template.replace('--DESC--', "[no description]")

    # AUTHOR ---
    
    if "author" in descriptor:
        authorlist = ""
        if isinstance(descriptor["author"], str):
            authorlist = authortemplate.replace("--NAME--", descriptor["author"])
            if descriptor["author"].startswith("http"):
                authorlist = authortemplate.replace("--URL--", descriptor["author"])
            else:
                authorlist = authortemplate.replace("--URL--", "")                
            
        elif isinstance(descriptor["author"], dict):
            if "name" in descriptor["author"]:
                authorlist = authortemplate.replace("--NAME--", descriptor["author"]["name"])
            else:
                authorlist = authortemplate.replace("--NAME--", "unnamed")
                
            aurl = ""
            
            if "id" in descriptor["author"]:
                aurl = descriptor["author"]["id"]
            elif "website" in descriptor["author"]:
                aurl = descriptor["author"]["website"]
            elif "email" in descriptor["author"]:
                aurl = descriptor["author"]["email"]
                
            if isinstance(aurl, list):
                aurl = aurl[0]

            authorlist = authortemplate.replace("--URL--", aurl)  

        else:
            if len(descriptor["author"]) > 1 :
                byline = byline.replace("Author", "Authors")
            for a in descriptor["author"]:
                if isinstance(a, str):
                    if a.startswith("http"):
                        aa = authortemplate.replace("--URL--", a)
                    else:
                        aa = authortemplate.replace("--URL--", "")
                    aa = aa.replace("--NAME--", a)
                else:
                    aurl = ""
                    if "id" in a:
                        aurl = a["id"]
                    elif "website" in a:
                        aurl = a["website"]
                    elif "email" in a:
                        aurl = a["email"]
                    if isinstance(aurl, list):
                        aurl = aurl[0]
                    aa = authortemplate.replace("--URL--", aurl)
                    aa = aa.replace("--NAME--", a["name"])
                        
                authorlist = authorlist + aa + ", "
            authorlist = authorlist[:-2]
                    
        byline = byline.replace("--AUTHORS--", authorlist)
        template = template.replace("<!-- BYLINE -->", byline)

        if "authorialContext" in descriptor:
            acontext = descriptor["authorialContext"]
            contextline = '<p>Authorial Context: <a href="--X--" rel="authorialContext">--X--</a></p>'
            if isinstance(acontext, str):
                contextline = contextline.replace("--X--", acontext)
                template = template.replace("<!-- AUTHORIALCONTXT -->", contextline)
            
    respheader = SERVER_VER + "Content-Type: text/html; charset=UTF-8\n\n"
    sys.stdout.buffer.write(respheader.encode('utf-8'))
    sys.stdout.buffer.write(template.encode('utf-8'))
    
def imageviewer(meta):
    # Generate a viewer page for an image resource.
    
    template = open('templates/image.html',mode='r',encoding='utf-8').read()
    
    imageUrl = urlroot + urlpath # + "?fragment=" + "0.0,0.0-1.0,1.0"
    indexDir = urlroot + urlpath[0:lastindexof("/",urlpath)] + "/"

    if "links" in meta.keys():
        linkmeta = { "links" : meta["links"]}
        del meta["links"]
    else:
        linkmeta = { "links" : []}

    meta = setEphemeralMeta(meta)
        
    if "name" in meta["descriptor"].keys():
        namemeta = meta["descriptor"]["name"]
        if isinstance(namemeta,list):
            title = ""
            plainTitle = namemeta[0]["@value"]
            for n in namemeta:
                title += '<small><sup>' + n["@language"] + ":</sup></small>" + n["@value"] + ('<br>' if (len(namemeta) > 1) else '')
        else:
            title = plainTitle = str(namemeta)
    else:
        title = plainTitle = "[" + os.path.basename(docpath) + "]"

    if "description" in meta["descriptor"].keys():
        descmeta = meta["descriptor"]["description"]
        if isinstance(descmeta,list):
            desc = ""
            for n in descmeta:
                desc += '<small><sup>' + n["@language"] + "</small></sup>:" + n["@value"] + ('<br>' if (len(descmeta) > 1) else '')
        else:
            desc = str(descmeta)
    else:
        desc = "[no description]"

    if "copyrightHolder" in meta["descriptor"].keys():
        crholder = meta["descriptor"]["copyrightHolder"]
        copyRight = "© "
        if "copyrightYear" in meta["descriptor"].keys():
            copyRight += str(meta["descriptor"]["copyrightYear"]) + " "
        if "url" in crholder.keys():
            copyRight += '<a href="' + crholder["url"] + '">' + crholder["name"] + '</a> '
        else:
            copyRight += crholder["name"] + " "
            
        if "license" in meta["descriptor"].keys():
            if type(meta["descriptor"]["license"]) != dict:
                copyRight += '<small>(<a href="' + meta["descriptor"]["license"] + '">license</a>)</small>'
            else:
                copyRight += meta["descriptor"]["license"]
    else:
        copyRight = "[no copyright information]"
    
    authors = "<ul>"
    if "author" in meta["descriptor"].keys():
        for a in meta["descriptor"]["author"]:
            authors += "<li>"
            if "url" in a.keys():
                authors += '<a href="' + a["url"] + '" rel="contact">' + a["name"]
            elif "email" in a.keys():
                mailprot = "" if a["email"].startswith("mailto:") else "mailto:"
                authors += '<a href="' + mailprot + a["email"] + '" rel="contact">' + a["name"]
            else:
                authors += a["name"]
            authors += '</a>'
            if "role" in a.keys():
                authors += ' (' + a["role"] + ')'
            authors += '</li>'
    authors += '</ul>'

    guts = '<h1>' + title + '</h1>' + authors + '<p>' + desc + '</p>' + '<aside>' + copyRight + '</aside>'
    
    template = template.replace('<!-- GUTS -->', guts)
    template = template.replace('<!-- URL -->', urlroot + urlpath)
    template = template.replace('<!-- IMAGE -->', imageUrl)
    template = template.replace('<!-- TITLE -->', title)
    template = template.replace('<!-- TITLETEXT -->', plainTitle)
    template = template.replace('<!-- DESC -->', desc)
    template = template.replace('<!-- WIDTH -->', str(int(meta["descriptor"]["width"].split()[0])))
    template = template.replace('<!-- HEIGHT -->', str(int(meta["descriptor"]["height"].split()[0])))
    #template = template.replace('<!-- METADATA -->', json.dumps(meta, ensure_ascii=False, indent=4))
    template = template.replace('<!-- METADATA -->', objectToHTML(meta["descriptor"]))
    #template = template.replace('<!-- LINKS -->', json.dumps(linkmeta, ensure_ascii=False, indent=4))
    template = template.replace('<!-- LINKS -->', objectToHTML(linkmeta))
    template = template.replace('<!-- INDEX -->', indexDir)
        
    #print("Content-Type: text/html; charset=UTF-8\n\n" + codestring)
    respheader = SERVER_VER + "Content-Type: text/html; charset=UTF-8\n\n"
    sys.stdout.buffer.write(respheader.encode('utf-8'))
    sys.stdout.buffer.write(template.encode('utf-8'))

def audioviewer(meta):
    # Generate a viewer page for an image resource.
    
    template = open('templates/audio.html',mode='r',encoding='utf-8').read()
    
    imageUrl = urlroot + urlpath # + "?fragment=" + "0.0,0.0-1.0,1.0"
    indexDir = urlroot + urlpath[0:lastindexof("/",urlpath)] + "/"

    if "links" in meta.keys():
        linkmeta = { "links" : meta["links"]}
        del meta["links"]
    else:
        linkmeta = { "links" : []}

    meta = setEphemeralMeta(meta)
        
    if "name" in meta["descriptor"].keys():
        namemeta = meta["descriptor"]["name"]
        if isinstance(namemeta,list):
            title = ""
            plainTitle = namemeta[0]["@value"]
            for n in namemeta:
                title += '<small><sup>' + n["@language"] + ":</sup></small>" + n["@value"] + ('<br>' if (len(namemeta) > 1) else '')
        else:
            title = plainTitle = str(namemeta)
    else:
        title = plainTitle = "[" + os.path.basename(docpath) + "]"

    if "description" in meta["descriptor"].keys():
        descmeta = meta["descriptor"]["description"]
        if isinstance(descmeta,list):
            desc = ""
            for n in descmeta:
                desc += '<small><sup>' + n["@language"] + "</small></sup>:" + n["@value"] + ('<br>' if (len(descmeta) > 1) else '')
        else:
            desc = str(descmeta)
    else:
        desc = "[no description]"

    if "copyrightHolder" in meta["descriptor"].keys():
        crholder = meta["descriptor"]["copyrightHolder"]
        copyRight = "© "
        if "copyrightYear" in meta["descriptor"].keys():
            copyRight += str(meta["descriptor"]["copyrightYear"]) + " "
        if "url" in crholder.keys():
            copyRight += '<a href="' + crholder["url"] + '">' + crholder["name"] + '</a> '
        else:
            copyRight += crholder["name"] + " "
            
        if "license" in meta["descriptor"].keys():
            if type(meta["descriptor"]["license"]) != dict:
                copyRight += '<small>(<a href="' + meta["descriptor"]["license"] + '">license</a>)</small>'
            else:
                copyRight += meta["descriptor"]["license"]
    else:
        copyRight = "[no copyright information]"
    
    authors = "<ul>"
    if "author" in meta["descriptor"].keys():
        for a in meta["descriptor"]["author"]:
            authors += "<li>"
            if "url" in a.keys():
                authors += '<a href="' + a["url"] + '" rel="contact">' + a["name"]
            elif "email" in a.keys():
                mailprot = "" if a["email"].startswith("mailto:") else "mailto:"
                authors += '<a href="' + mailprot + a["email"] + '" rel="contact">' + a["name"]
            else:
                authors += a["name"]
            authors += '</a>'
            if "role" in a.keys():
                authors += ' (' + a["role"] + ')'
            authors += '</li>'
    authors += '</ul>'

    guts = '<h1>' + title + '</h1>' + authors + '<p>' + desc + '</p>' + '<aside>' + copyRight + '</aside>'
    
    template = template.replace('<!-- GUTS -->', guts)
    template = template.replace('<!-- URL -->', urlroot + urlpath)
    template = template.replace('<!-- TITLE -->', title)
    template = template.replace('<!-- TITLETEXT -->', plainTitle)
    template = template.replace('<!-- DESC -->', desc)
    template = template.replace('<!-- METADATA -->', objectToHTML(meta["descriptor"]))
    template = template.replace('<!-- LINKS -->', objectToHTML(linkmeta))
    template = template.replace('<!-- INDEX -->', indexDir)
        
    respheader = SERVER_VER + "Content-Type: text/html; charset=UTF-8\n\n"
    sys.stdout.buffer.write(respheader.encode('utf-8'))
    sys.stdout.buffer.write(template.encode('utf-8'))

def videoviewer(meta):
    return textResponse("ALAS,\nA VIEWER IS NOT YET WRITTEN FOR THIS MEDIA TYPE.\n\nWE SINCERELY APOLOGIZE.\n\n\nHOW DID YOU GET HERE, ANYWAY?")

def blobviewer(meta):
    return textResponse("ALAS,\nA VIEWER IS NOT YET WRITTEN FOR THIS MEDIA TYPE.\n\nWE SINCERELY APOLOGIZE.\n\n\nHOW DID YOU GET HERE, ANYWAY?")

def compileParentMeta(meta):
    # Make a list of parent paths to check for metadata
    pathlist = []
    parentpath = docpath[0:lastindexof("/",docpath)]
    while parentpath != docroot:
        pathlist.append(parentpath)
        parentpath = docpath[0:lastindexof("/",parentpath)]

    while len(pathlist):
        parentpath = pathlist.pop()
        if os.path.exists(parentpath + "/.meta"):
            dirmeta = json.load(open(parentpath + "/.meta",encoding='utf-8'))
            meta = metamerge(meta,dirmeta)

    return meta

def makeMediaMeta(meta,f):
    global debugtext

    if f:
        docpath = f
        
    mimestring = subprocess.check_output(['file','--mime', docpath]).decode('utf-8').split(":")[1].strip()
    mimetype = mimestring.split(";")[0]
    encoding = mimestring.split(";")[1].strip()
    extension = docpath.split(".")[-1]

    #meta["alph:mediaInterfaces"] = [ "http://alph.io/interfaces/describe",
    #                                 "http://alph.io/interfaces/link",
    #                                 "http://alph.io/interfaces/fragment"]
    
    # 'file' doesn't know what to do with a lot of plain text formats, so force some here.
    if mimestring.startswith("text"):
        if extension.lower() == "css":
            mimetype = "text/css"
        elif extension.lower() == "jsonld":
            mimetype = "application/ld+json"
        elif extension.lower() == "json":
            mimetype = "application/json"            
        elif extension.lower() == "alink":
            mimetype = "application/alink+json"
            
        mimestring = mimetype + "; " + encoding

    if mimestring.startswith("video") or mimestring.startswith("audio"):
        # Call exiftool if it's video/audio, because 'file' gets the MIME type wrong
        # on WebM files. ANNOYING.
        mimestring = subprocess.check_output(['exiftool','-mimetype','-S',docpath]).decode('utf-8').split(":")[1].strip()
        if mimestring.startswith("audio/x-matr"):
            mimestring = "audio/webm"

    meta["fileFormat"] = mimestring
    if meta["fileFormat"].startswith("text"):
        # Read the file's length (in codepoints). For this, We use
        # Awk, because its behaviour is consistent and it doesn't do
        # any automatic newline conversion -- UNLIKE A CERTAIN
        # LANGUAGE I KNOW.
        awk = subprocess.Popen(['awk',
                                'BEGIN { RS="\x04"; ORS="" } { print length($0); }',
                                docpath],
                               stdout=subprocess.PIPE)
        length = int(awk.communicate()[0].decode('utf-8').strip())
        #meta["alph:fragmentBounds"] = {
        #    "http://alph.io/fragmentSyntaxes/integerRange": "0-" + str(length) }
        #length = len(open(docpath, encoding='utf-8').read())
        meta["alph:textLength"] = length
        
    elif meta["fileFormat"].startswith("image"):
        magick = subprocess.check_output(['identify','-format','%W,%H,%r,%z',docpath])
        image_specs = magick.decode('utf-8').split(",")
        meta["width"] = image_specs[0].strip() + " px"
        meta['height'] = image_specs[1].strip() + " px"
        #meta['pixel_format'] = image_specs[2]
        #meta["alph:fragmentBounds"] = {
        #    "http://alph.io/fragmentSyntaxes/normalizedRectangle":"0.0,0.0-1.0,1.0",
        #    "http://alph.io/fragmentSyntaxes/integerRectangle": "0,0-" + image_specs[0] + "," + image_specs[1]}
        
    elif meta["fileFormat"].startswith("audio"):
        probemeta = json.loads(subprocess.check_output(['ffprobe',
                                                        '-of','json',
                                                        '-show_format',
                                                        '-show_streams',
                                                        docpath]).decode('utf-8'),
                               encoding='utf-8')
        # avprobe is fucking verbose, so its output should probably be
        # massaged/edited into something more appropriate to the simple task
        # of transclusion.

        # It's verbose, but it's so useful. Is there a way that we can make use of
        # all of this metadata while still issuing valid JSON-LD?
        
        #meta["format"] = probemeta["format"]
        #del meta["format"]["filename"] # Don't expose the server's filesystem, plz
        #meta["streams"] = probemeta["streams"]
        
        secs = float(probemeta["format"]["duration"])
        meta["duration"] = datetime.time(int(secs/3600),
                                   int((secs % 3600)/60),
                                   int(secs % 60),
                                   int(1000000*(secs - int(secs))),
                                   tzinfo=None).isoformat()
        #meta["alph:fragmentBounds"] = {
        #    "http://alph.io/fragmentSyntaxes/floatRange":"0.0-" + str(secs)}
        
    elif meta["fileFormat"].startswith("video"):
        # Now... avprobe is a real pain in the ass when it comes to getting the frame-rate
        # of a video. The only semi-reliable value is the "tbr" value which is shown in
        # avprobe's stderr output. So, as useful as all of that JSON crap is, we still
        # have to regex the tbr value out of the console output.
        
        p = subprocess.Popen(['ffprobe',
                               '-of','json',
                               '-show_format',
                               '-show_streams',
                               docpath],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        probeout , probeerr = p.communicate()
        probeout.decode('utf-8')
        probemeta = json.loads(probeout)
        probeerr = probeerr.decode('utf-8')
        tbr = re.search('fps,(?P<value>.*)?tbr,',probeerr)
        if tbr: tbr = float(tbr.group('value').strip())
        else: tbr = probemeta["streams"](0)["avg_frame_rate"]
        #meta["frames"] = int(float(probemeta["format"]["duration"]) * tbr)
        #meta["frame_rate"] = tbr
        #meta["format"] = probemeta["format"]
        #del meta["format"]["filename"] # Don't expose the server's filesystem, plz
        #meta["streams"] = probemeta["streams"]
        secs = float(probemeta["format"]["duration"])
        meta["duration"] = datetime.time(int(secs/3600),
                                   int((secs % 3600)/60),
                                   int(secs % 60),
                                   int(1000000*(secs - int(secs))),
                                   tzinfo=None).isoformat()
        
        #meta["alph:fragmentBounds"] = {
        #    "http://alph.io/fragmentSyntaxes/floatRange":"0.0-"+ str(secs)}
    else:
        length = len(open(docpath,mode='rb').read())
        meta["alph:textLength"] = length
        #meta["alph:fragmentBounds"] = {
        #    "http://alph.io/fragmentSyntaxes/integerRange":"0-"+str(length)}
    return meta

def metaEditor(meta):
    # remove all of the automatically-generated
    # metadata which can't be changed anyway.
    ineditableMeta = {}
    for ineditable in ["@id",
                       "@context",
                       "@type",
                       "fileFormat",
                       "width",
                       "height",
                       "duration",
                       "alph:textLength",
                       "alph:fragmentBounds",
                       "alph:mediaInterfaces"]:
        if ineditable in meta["descriptor"].keys():
            ineditableMeta[ineditable] = meta["descriptor"][ineditable]
            del meta["descriptor"][ineditable]
        
    template = open('templates/metaEditor.html',mode='r',encoding='utf-8').read()
    template = template.replace("<!-- METAJSON -->",
                                json.dumps(meta["descriptor"], ensure_ascii=False, indent=4))
    template = template.replace("<!-- FIXEDMETA -->",
                                json.dumps(ineditableMeta, ensure_ascii=False, indent = 4))
    template = template.replace("<!-- URLPATH -->",urlpath)
    template = template.replace("<!-- URLROOT -->",urlroot)
    respheader = SERVER_VER + "Content-type: text/html; charset=UTF-8\n\n"
    sys.stdout.buffer.write(respheader.encode('utf-8'))
    sys.stdout.buffer.write(template.encode('utf-8'))
    

def linkIfLink(s):
    if s.startswith("http://") or s.startswith("https://"):
        return '<a href="' + s + '">' + s + '</a>'
    else:
        return s
    
def objectToHTML(o):
    
    if type(o) == type([]):
        codestring = '<ul class="json-array">'
        if len(o) > 0:
            for el in o:
                codestring += '<li>'
                if type(el) == type([]):
                    codestring += objectToHTML(el)
                elif type(el) == type({}):
                    codestring += objectToHTML(el)
                else:
                    codestring += '<span class="json-value">' + linkIfLink(str(el)) + '</span>'
                codestring += '</li>'
    elif type(o) == type({}):
        codestring = '<ul class="json-object">'
        keys = o.keys()
        if keys:
            for key in keys:
                codestring += "<li>"
                codestring += '<span class="json-key">' + linkIfLink(key) + '</span> &rarr; '
                if type(o[key]) == type([]):
                    codestring += objectToHTML(o[key])
                elif type(o[key]) == type({}):
                    codestring += objectToHTML(o[key])
                else:
                    codestring += '<span class="json-value">' + linkIfLink(str(o[key])) + '</span>'
                codestring += '</li>'
    codestring += "</ul>"
    return codestring

def textResponse(text):
    text = SERVER_VER + "Content-type: text/plain; charset=UTF-8\n\n" + text
    sys.stdout.buffer.write(text.encode('utf-8'))

def errorResponse(code,text):
    # Wait, wait, wait. There's something that I'm not understanding about authenticated
    # HTTP, here. If I throw an error in the middle of a 401 auth request, the client
    # never asks the user for credentials and the thing fails. Disabling this for now.

    header = "Status: " + str(code) + "\n" + SERVER_VER + "Content-type: text/plain; charset=UTF-8\n\n"
    text = header + text
    sys.stdout.buffer.write(text.encode('utf-8'))
    
def hunt():
    if os.environ.get('HTTP_REFERER'):
        linkhunt = subprocess.Popen(['./linker.py',urlpath,os.environ.get('HTTP_REFERER')],stdout=subprocess.PIPE)
        linkhuntoutput = linkhunt.communicate()[0]
    
# -------------------------------------------------------------
main()
