#!/usr/bin/env python3

# linker.py - Backlink search thingie.
#              ------------------------
#              The Alph Project <http://alph.io>

# Call me with the Web path (without protocol and server name) of a
# resource on this server and the URL of a remote file to be scanned
# for links.


import sys, os, subprocess, urllib.request, re, json, datetime
from xml.dom import minidom
import html5lib
import Config
from Config import docroot, urlroot

#import cgitb
#cgitb.enable(display=0, logdir="./dustbin/")


oldlinkcount = 0
strippedlinkcount = 0

def elementTreeToArray(root):
    # Given an HTML element tree, I return a flat array of the elements in that tree
    children = []
    if root.childNodes:
        for node in root.childNodes:
           if node.nodeType == node.ELEMENT_NODE:
               children.append(node)
               elementTreeToArray(node)
    return children


def catElementSpans(elements):
    # Given a flat array of HTML elements, I will return an object array of
    # content URL strings, concatenating transclusion specs of same-source spans 
    response = ""
    lastxsrc = ""
    for el in elements:        
        xsrc = el.get("src")
        if xsrc and el.get("origin"):
            # <x-text> elements
            if xsrc == lastxsrc:
                response += "`" + el.get("origin") + "-" + el.get("extent")
            else:
                response += ";" + xsrc + "?fragment=" + el.get("origin") + "-" + el.get("extent")
            lastxsrc = xsrc
        elif xsrc:
            lastxsrc = ""
            response += ";" + xsrc
    return response[1:].split(";")

def getSelectors(element,docurl=False):
    specs = []
    for el in elementTreeToArray(element):
        # Text transclusions
        if el.tagName == "x-text":
            xsrc = el.getAttribute("src")
            if not docurl or xsrc.startswith(docurl):
                specs.append({"type":"TextPositionSelector",
                              "source":xsrc,
                              "start":el.getAttribute("origin"),
                              "end":el.getAttribute("extent")})
        # Image transclusion
        elif el.tagName == "img":
            src = el.getAttribute("src")        
            if not docurl or src.split("?")[0] == docurl:
                selector = {"type":"FragmentSelector",
                            "source":src.split("?")}
                if src.find("fragment=") > 0:
                    selstring = src.split("fragment=")[1]
                    selstring = selstring.split("&")[0]
                    if selstring.find(".") > 1:
                        selector["conformsTo"] = "http://alph.io/normalizedRectangle"
                    else:
                        selector["conformsTo"] = "http://alph.io/integerRectangle"
                    
                    selector["value"] = selstring
                else:
                    selector["conformsTo"] = "http://alph.io/normalizedRectangle"
                    selector["value"] = "0.0,0.0-1.0,1.0"

                specs.append(selector)

    return specs

def getSelectors_lxml(element,docurl=False):
    specs = []
    for el in element.iterdescendants():
        # Text transclusions
        if el.tagName == "x-text":
            xsrc = el.get("src")
            if not docurl or xsrc.startswith(docurl):
                specs.append({"type":"TextPositionSelector",
                              "source":xsrc,
                              "start":el.get("origin"),
                              "end":el.get("extent")})
        # Image transclusion
        elif el.tag == "img":
            src = el.get("src")        
            if not docurl or src.split("?")[0] == docurl:
                selector = {"type":"FragmentSelector",
                            "source":src.split("?")}
                if src.find("fragment=") > 0:
                    selstring = src.split("fragment=")[1]
                    selstring = selstring.split("&")[0]
                    if selstring.find(".") > 1:
                        selector["conformsTo"] = "http://alph.io/normalizedRectangle"
                    else:
                        selector["conformsTo"] = "http://alph.io/integerRectangle"
                    
                    selector["value"] = selstring
                else:
                    selector["conformsTo"] = "http://alph.io/normalizedRectangle"
                    selector["value"] = "0.0,0.0-1.0,1.0"

                specs.append(selector)

    return specs


def main(subject,target,output):
    if output:
        print("Alph.py Backlinker Report\n\n")
    else:
        output = False;
        
    docpath = docroot + subject
    docurl = urlroot + subject
    vtime = datetime.datetime.now()
    global strippedlinkcount
    global oldlinkcount

    if os.path.exists(docpath + ".meta"):
        metafile = open(docpath + ".meta",mode='r',encoding='utf-8')
        meta = json.load(metafile)
        metafile.close()
    else:
        # If, somehow, this file exists on the server but it doesn't have
        # a metadata file yet, create one. This shouldn't ever happen, but...
        meta = {
        "descriptor":{
            "@context":"http://schema.org",
            "type":"MediaObject"
            },
        "links":[]
        }

    if output:
        print("Searching " + target + " for links to " + urlroot + subject + ":\n")

        
    # Strip any pre-existing links for this document from the meta.
    
    goodlinks = []
    targetname = target.split("?")[0].split("#")[0]
    oldlinkcount = len(meta["links"])
    for link in meta["links"]:
        if "id" in link.keys():
            if link["id"].split("?")[0].split("#")[0] != targetname:
                goodlinks.append(link)
            else:
                # Check the last time we scanned this resource. If it was
                # less than a minute ago, abort the scan.
                linkvtime = datetime.datetime.strptime(link["verified"].split(".")[0],
                                                       "%Y-%m-%dT%H:%M:%S")
                d = vtime - linkvtime
                if d.seconds < 10:
                    if output:
                        print("Document was last scanned " + str(d.seconds) + "seconds ago. Please wait a moment and scan again.")
                    return
                
    meta["links"] = goodlinks
    strippedlinkcount = len(goodlinks)
    
    # Do a HEAD request to the resource to see if it returns something
    # we actually want.

    headreq = urllib.request.Request(target,method="HEAD")
    try:
        head = urllib.request.urlopen(headreq)
    except urllib.error.HTTPError as e:
        print("Unable to query resource.",e)
        return
        
    if head.getcode() != 200:
        # We want to be relatively harsh, here. If we don't get a status
        # 200 response, write the link meta with all references to the
        # document stripped. If we get a response, but it's not readable
        # as HTML, do the same. Perhaps I should be using some kind of
        # three-strikes rule, but for now if the resource isn't valid
        # at the time this script runs, we treat it like it's dead.

        if output:
            print("HTTP Status " + head.getcode() + ".\nAs the resource appears to be unreachable, any preexisting links in the registry shall be removed.")
        writeOut(meta,docpath)
        return

    # All we're supporting is HTML right now. 
    if not head.info()["Content-Type"].startswith("text/html"):
        if output:
            print("Linker cannot currently parse " + head.info()["Content-Type"] + " documents.")
        writeOut(meta,docpath)            
        return

    
    # If we got this far, it *is* a valid HTML resource. GET it...
    req = urllib.request.urlopen(target)    
    try:
        html = req.read().decode('utf-8')
        doc = html5lib.parse(html,treebuilder="dom")
        #lxml# dom = etree.HTML(html)
        #lxml# domtree = etree.ElementTree(dom)
    except AttributeError as e:
        if output:
            print("Problem parsing HTML file. No links registered.",e)
        writeOut(meta,docpath)
        return

    # Now we scan for links.
    
    # Any links we find, we want to store them in the link registry in a form
    # based on the Web Annotation data model, which is a very nice design, and
    # more mature than my previous Alph Link Descriptor format.

    # <a> links (weblinks and hyper/xanalinks)
    #lxml# anchors = dom.findall(".//a")
    anchors = doc.getElementsByTagName("a")
    
    #weblinkctr = 0  # Needed to prevent duplicate weblink occurrences.

    for anchor in anchors:
        #lxml# href = anchor.get("href")
        href = anchor.getAttribute("href")
        if href == '': continue
        if href.split("?")[0].split("#")[0] == docurl:
            hyperlink = {"verified":vtime.isoformat()}
            # For the link's "id", we want an IRI for the anchor itself, not
            # just the containing document. If the anchor has an 'id' attribute,
            # great! We'll use that. If it doesn't, get its XPath
            
            #lxml# if anchor.get("id") != None:
            if anchor.getAttribute("id") != '':
                hyperlink["id"] = target + "#" + anchor.getAttribute("id")
            else:
                #hyperlink["id"] = target + "#xpointer(" + domtree.getpath(anchor) + ")"
                hyperlink["id"] = target + "#" + getElementPointer(anchor,doc.documentElement)
                
            # Is the link to a fragment? Ugggghhh... this could/should be
            # a big thing that dereferences fragment URIs and yadda, yadda.
            # For our present purposes, we'll only look for ?fragment queries
            # and work on adding the other stuff later.
            
            if href.find("fragment=") > 0 :
                hyperlink["type"] = "deeplink"
                
                hyperlink["target"] = { "selector":[] }
                
                spans = href.split("fragment=")[1]
                spans = spans.split("&")[0]
                spans = spans.split("`")
                for span in spans:
                    selector = {"type":"TextPositionSelector"}
                    selector["start"],selector["end"] = span.split("-")
                    selector["start"] = int(selector["start"])
                    selector["end"] = int(selector["end"])
                    selector["source"] = docurl
                    hyperlink["target"]["selector"].append(selector)
                
                bodycontent = getSelectors(anchor)
                if not len(bodycontent) > 0:
                    #lxml# bodycontent = ""
                    #lxml# for t in anchor.itertext():
                    #lxml#    bodycontent += t
                    bodycontent = anchor.textContent;
                    hyperlink["body"] = {"type":"TextualBody",
                                         "value": bodycontent,
                                         "format":"text/plain"}
                else:
                    hyperlink["body"] = {"selector": bodycontent }
            else:
                hyperlink["type"] = "weblink"
                #weblinkctr = weblinkctr + 1

            if anchor.getAttribute("rel"):                
                #lxml# hyperlink["rel"] = anchor.get("rel")
                hyperlink["rel"] = anchor.getAttribute("rel")                
            if anchor.getAttribute("title"):
                #lxml# hyperlink["title"] = anchor.get("title")
                hyperlink["title"] = anchor.getAttribute("title")                
                

            #if hyperlink["type"] == "deeplink" or weblinkctr == 1:
            #    meta["links"].append(hyperlink)
            #    if output:
            #        print("Added " + json.dumps(hyperlink))
            #else:
            #    if output:
            #        print("Skipping duplicate weblink...")
            
            meta["links"].append(hyperlink)
            if output:
                print("Added " + json.dumps(hyperlink))

                
    # <x-text src=...>  text transclusions
    
    specs = []
    #lxml# for span in dom.iterdescendants(tag="x-text"):
    for span in doc.getElementsByTagName("x-text"):
        #lxml# xsrc = span.get("src")
        xsrc = span.getAttribute("src")
        if xsrc:
            if xsrc.startswith(docurl):
                specs.append({"type":"TextPositionSelector",
                              "source":xsrc,
                              #lxml# "start":span.get("origin"),
                              #lxml# "end":span.get("extent")})
                              "start":span.getAttribute("origin"),
                              "end":span.getAttribute("extent")})
    if len(specs) > 0:
        transclusion = { "id" : target,
                         "verified": vtime.isoformat(),
                         "type":"transclusion",
                         "target":{
                             "selector":specs
                         }
        }

        meta["links"].append(transclusion)
        if output:
            print("Added " + json.dumps(transclusion))
        
        
    # <img> transclusions
    #lxml# for img in dom.iterdescendants(tag="img"):
    for img in doc.getElementsByTagName("img"):        
        src = img.getAttribute("src")
        #lxml# src = img.get("src")        
        if src.split("?")[0] == docurl:
            imglink = {"id":target,
                       "verified":vtime.isoformat(),
                       "type":"transclusion",
                       "target":{}
            }
            selector = {"type":"FragmentSelector"}
            if src.find("fragment=") > 0:
                src = src.split("fragment=")[1]
                src = src.split("&")[0]
                if src.find(".") > 1:
                    selector["conformsTo"] = "http://alph.io/normalizedRectangle"
                else:
                    selector["conformsTo"] = "http://alph.io/integerRectangle"
                    
                selector["value"] = src
                imglink["target"]["selector"] = selector
            else:
                selector["conformsTo"] = "http://alph.io/normalizedRectangle"
                selector["value"] = "0.0,0.0-1.0,1.0"
                imglink["target"]["selector"] = selector
            meta["links"].append(imglink)
            if output:
                print("Found " + json.dumps(imglink))
            
    # <source>
    # ...
    
    # <video>
    # ...
    
    # <audio>
    # ...:

    # <head>
    # We need to look in the <head> for <link> elements with a
    # rel="xanalink", then fire the linker again (or a new linker?
    # Probably a good idea) for those xanalogical link documents.
    
    writeOut(meta,docpath)
    
    if output:
        print("\nLink hunt complete.\nUpdated link registry at " + urlroot + subject + "?link")
    #if len(meta["links"]) > 0
    
def writeOut(meta,dpath):
    if len(meta["links"]) != oldlinkcount or len(meta["links"]) != strippedlinkcount:
        json.dump(meta,open(dpath + ".meta",mode='w',encoding='utf-8'))
    return

def getElementPointer(node,rootNode):
    path = ""
    while node.parentNode and node != rootNode:
        if node.getAttribute("id") != '':
            path = 'element("' + node.getAttribute("id") + '"' + path + ")"
            return path
        else:
            sibs = node.parentNode.childNodes
            nodix = 0
            for ix in sibs:
                nodix = nodix + 1
                if ix == node:
                    path = "/" + str(nodix) + path
                    break
        node = node.parentNode
    return "element(" + path[1:] + ")"

# -------------------------------------------------------------------------
if len(sys.argv) == 3:
    main(sys.argv[1],sys.argv[2],False)
elif len(sys.argv) == 4:
    main(sys.argv[1],sys.argv[2],True)
else:
    print("Insufficient arguments.")
